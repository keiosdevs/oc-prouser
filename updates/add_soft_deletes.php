<?php namespace Keios\ProUser\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateCountriesTable
 * @package Keios\ProUser\Updates
 */
class AddSoftDeletes extends Migration
{

    public function up()
    {
        Schema::table('keios_prouser_users', function(Blueprint $table)
        {
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::table('keios_prouser_users', function(Blueprint $table)
        {
            $table->dropSoftDeletes();
        });
    }

}
